import uiScriptLocale

window = {
	"name" : "LoginWindow",
	"sytle" : ("movable",),

	"x" : 0,
	"y" : 0,

	"width" : SCREEN_WIDTH,
	"height" : SCREEN_HEIGHT,

	"children" :
	(
		## Board
		{
			"name" : "BackGround",
			"type" : "expanded_image",

			"x" : 0,
			"y" : 0,

			"image" : "interface/intro/pattern/Line_Pattern.tga",

			"x_scale" : float(SCREEN_WIDTH) / 800.0,
			"y_scale" : float(SCREEN_HEIGHT) / 600.0,
		},
		{ 
			"name":"ErrorMessage", 
			"type":"text", "x":10, "y":10, 
			"text": uiScriptLocale.LOAD_ERROR, 
		},
		{
			"name" : "GageBoard",
			"type" : "window",
			"x" : float(SCREEN_WIDTH) * 400 / 800.0 - 200,
			"y" : float(SCREEN_HEIGHT) * 500 / 600.0 ,
			"width" : 400, 
			"height": 80,

			"children" :
			(
				{
					"name" : "BackGage",
					"type" : "ani_image",


					"x" : 0,
					"y" : 0,

					"delay" : 1,

					"images" :
					(
						"interface/intro/loading/00.sub",
						"interface/intro/loading/01.sub",
						"interface/intro/loading/02.sub",
						"interface/intro/loading/03.sub",
						"interface/intro/loading/04.sub",
						"interface/intro/loading/05.sub",
						"interface/intro/loading/06.sub",
						"interface/intro/loading/07.sub",
						"interface/intro/loading/08.sub",
						"interface/intro/loading/09.sub",
						"interface/intro/loading/10.sub",
						"interface/intro/loading/11.sub",						
						"interface/intro/loading/12.sub",
						"interface/intro/loading/13.sub",
						"interface/intro/loading/14.sub",
						"interface/intro/loading/15.sub",
						"interface/intro/loading/16.sub",
						"interface/intro/loading/17.sub",
						"interface/intro/loading/18.sub",
						"interface/intro/loading/19.sub",
						"interface/intro/loading/20.sub",
						"interface/intro/loading/21.sub",
						"interface/intro/loading/22.sub",
						"interface/intro/loading/23.sub",
					)
				},
				{
					"name" : "BackGage",
					"type" : "expanded_image",

					"x" : 70,
					"y" : 25,

					"image" : "interface/intro/loading/gauge_empty.sub",			
				},
				{
					"name" : "FullGage",
					"type" : "expanded_image",

					"x" : 70,
					"y" : 25,

					"image" : "interface/intro/loading/gauge_full.sub",			
				},
			),
		},
	),
}
