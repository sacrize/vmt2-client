import uiScriptLocale
import item
import app

EQUIPMENT_START_INDEX = 450

window = {
	"name" : "InventoryWindow",

	## 600 - (width + 오른쪽으로 부터 띄우기 24 px)
	"x" : SCREEN_WIDTH - 176,
	"y" : SCREEN_HEIGHT - 37 - 584,

	"style" : ("movable", "float",),

	"width" : 176,
	"height" : 584,

	"children" :
	(
		## Inventory, Equipment Slots
		{
			"name" : "board",
			"type" : "board",
			"style" : ("attach",),

			"x" : 0,
			"y" : 0,

			"width" : 176,
			"height" : 584,

			"children" :
			(
				## Title
				{
					"name" : "TitleBar",
					"type" : "titlebar",
					"style" : ("attach",),

					"x" : 8,
					"y" : 7,

					"width" : 161,
					"color" : "yellow",

					"children" :
					(
						{ "name":"TitleName", "type":"text", "x":77, "y":3, "text":uiScriptLocale.INVENTORY_TITLE, "text_horizontal_align":"center" },
					),
				},

				## Equipment Slot
				{
					"name" : "Equipment_Base",
					"type" : "image",

					"x" : 10,
					"y" : 33,

					"image" : "interface/equipment_bg_without_ring.tga",

					"children" :
					(

						{
							"name" : "EquipmentSlot",
							"type" : "slot",

							"x" : 3,
							"y" : 3,

							"width" : 150,
							"height" : 182,

							"slot" : (
									{"index":EQUIPMENT_START_INDEX+0, "x":39, "y":37, "width":32, "height":64},
									{"index":EQUIPMENT_START_INDEX+1, "x":39, "y":2, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+2, "x":39, "y":145, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+3, "x":75, "y":67, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+4, "x":3, "y":3, "width":32, "height":96},
									{"index":EQUIPMENT_START_INDEX+5, "x":114, "y":67, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+6, "x":114, "y":35, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+7, "x":2, "y":145, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+8, "x":75, "y":145, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+9, "x":114, "y":2, "width":32, "height":32},
									{"index":EQUIPMENT_START_INDEX+10, "x":75, "y":35, "width":32, "height":32},
									## 새 반지1
									##{"index":item.EQUIPMENT_RING1, "x":2, "y":106, "width":32, "height":32},
									## 새 반지2
									##{"index":item.EQUIPMENT_RING2, "x":75, "y":106, "width":32, "height":32},
									## 새 벨트
									{"index":item.EQUIPMENT_BELT, "x":39, "y":106, "width":32, "height":32},
								),
							},
							## Dragon Soul Button
#							{
#								"name" : "DSSButton",
#								"type" : "button",

#								"x" : 114,
#								"y" : 107,

#								"tooltip_text" : uiScriptLocale.TASKBAR_DRAGON_SOUL,

#								"default_image" : "interface/dragonsoul/dss_inventory_button_01.tga",
#								"over_image" : "interface/dragonsoul/dss_inventory_button_02.tga",
#								"down_image" : "interface/dragonsoul/dss_inventory_button_03.tga",
#							},
#							## MallButton
#							{
#								"name" : "MallButton",
#								"type" : "button",
#
#								"x" : 118,
#								"y" : 148,
#
#								"tooltip_text" : uiScriptLocale.MALL_TITLE,
#
#								"default_image" : "interface/game/TaskBar/Mall_Button_01.tga",
#								"over_image" : "interface/game/TaskBar/Mall_Button_02.tga",
#								"down_image" : "interface/game/TaskBar/Mall_Button_03.tga",
#							},
						## CostumeButton
						{
							"name" : "CostumeButton",
							"type" : "button",

							"x" : 78,
							"y" : 5,

							"tooltip_text" : uiScriptLocale.COSTUME_TITLE,

							"default_image" : "interface/game/taskbar/costume_Button_01.tga",
							"over_image" : "interface/game/taskbar/costume_Button_02.tga",
							"down_image" : "interface/game/taskbar/costume_Button_03.tga",
						},						
						{
							"name" : "Equipment_Tab_01",
							"type" : "radio_button",

							"x" : 86,
							"y" : 161,

							"default_image" : "interface/game/windows/tab_button_small_01.sub",
							"over_image" : "interface/game/windows/tab_button_small_02.sub",
							"down_image" : "interface/game/windows/tab_button_small_03.sub",

							"children" :
							(
								{
									"name" : "Equipment_Tab_01_Print",
									"type" : "text",
										
									"x" : 0,
									"y" : 0,

									"all_align" : "center",

									"text" : "I",
								},
							),
						},
						{
							"name" : "Equipment_Tab_02",
							"type" : "radio_button",

							"x" : 86 + 32,
							"y" : 161,

							"default_image" : "interface/game/windows/tab_button_small_01.sub",
							"over_image" : "interface/game/windows/tab_button_small_02.sub",
							"down_image" : "interface/game/windows/tab_button_small_03.sub",

							"children" :
							(
								{
									"name" : "Equipment_Tab_02_Print",
									"type" : "text",

									"x" : 0,
									"y" : 0,

									"all_align" : "center",

									"text" : "II",
								},
							),
						},
					),
				},

				{
					"name" : "Inventory_Tab_01",
					"type" : "radio_button",

					"x" : 8,
					"y" : 33 + 191,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "1. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_01_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,
								
							"all_align" : "center",
								
							"text" : "I",
						},
					),
				},
				{
					"name" : "Inventory_Tab_02",
					"type" : "radio_button",

					"x" : 8 + 32,
					"y" : 33 + 191,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "2. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_02_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "II",
						},
					),
				},
				{
					"name" : "Inventory_Tab_03",
					"type" : "radio_button",

					"x" : 8 + 64,
					"y" : 33 + 191,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "3. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_03_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "III",
						},
					),
				},
				{
					"name" : "Inventory_Tab_04",
					"type" : "radio_button",

					"x" : 8 + 96,
					"y" : 33 + 191,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "4. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "IV",
						},
					),
				},
				{
					"name" : "Inventory_Tab_05",
					"type" : "radio_button",

					"x" : 8 + 128,
					"y" : 33 + 191,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "5. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_05_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "V",
						},
					),
				},
				{
					"name" : "Inventory_Tab_06",
					"type" : "radio_button",

					"x" : 8,
					"y" : 33 + 210,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "6. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_06_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "VI",
						},
					),
				},
				{
					"name" : "Inventory_Tab_07",
					"type" : "radio_button",

					"x" : 8 + 32,
					"y" : 33 + 210,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "7. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_07_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "VII",
						},
					),
				},
				{
					"name" : "Inventory_Tab_08",
					"type" : "radio_button",

					"x" : 8 + 64,
					"y" : 33 + 210,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "8. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_08_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "VIII",
						},
					),
				},
				{
					"name" : "Inventory_Tab_09",
					"type" : "radio_button",

					"x" : 8 + 96,
					"y" : 33 + 210,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "9. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_09_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "IX",
						},
					),
				},
				{
					"name" : "Inventory_Tab_10",
					"type" : "radio_button",

					"x" : 8 + 128,
					"y" : 33 + 210,

					"default_image" : "interface/game/windows/tab_button_small_01.sub",
					"over_image" : "interface/game/windows/tab_button_small_02.sub",
					"down_image" : "interface/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "10. " + uiScriptLocale.INVENTORY_PAGE_BUTTON_TOOLTIP,

					"children" :
					(
						{
							"name" : "Inventory_Tab_10_Print",
							"type" : "text",

							"x" : 0,
							"y" : 0,

							"all_align" : "center",

							"text" : "X",
						},
					),
				},

				## Item Slot
				{
					"name" : "ItemSlot",
					"type" : "grid_table",
						
					"x" : 8,
					"y" : 246 + 19,

					"start_index" : 0,
					"x_count" : 5,
					"y_count" : 9,
					"x_step" : 32,
					"y_step" : 32,

					"image" : "interface/public/Slot_Base.sub"
				},

				## Print
				{
					"name":"Money_Slot",
					"type":"button",

					"x":8,
					"y":28,

					"horizontal_align":"center",
					"vertical_align":"bottom",

					"default_image" : "interface/public/parameter_slot_05.sub",
					"over_image" : "interface/public/parameter_slot_05.sub",
					"down_image" : "interface/public/parameter_slot_05.sub",

					"children" :
					(
						{
							"name":"Money_Icon",
							"type":"image",
								
							"x":-18,
							"y":2,

							"image":"interface/game/windows/money_icon.sub",
						},
							
						{
							"name" : "Money",
							"type" : "text",

							"x" : 3,
							"y" : 3,

							"horizontal_align" : "right",
							"text_horizontal_align" : "right",
								
							"text" : "123456789",
						},
					),
				},
			),
		},
	),
}
